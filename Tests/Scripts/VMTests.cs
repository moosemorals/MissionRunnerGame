using System;
using System.Collections.Generic;
using System.Linq;

using GameLibrary.Models;
using GameLibrary.Scripting;

using NUnit.Framework;

namespace Tests.Scripts {

    [TestFixture]
    public class VMTests {

        [Test]
        public void CommsCheck() {
            SimpleReporter reporter = new SimpleReporter();

            VM vm = new VM(reporter);

            string? pong = null;
            string toScript = "Hello";
            string fromScript = "world";

            ObjNative Ping = new ObjNative("ping",
                args => {
                    pong = ( args[0] as ObjString )?.Str;
                    return Value.FromString(new ObjString(toScript));
                },
                1);

            vm.DefineNative(Ping);
            string input = $"var a = ping(\"{fromScript}\"); print a;";
            ObjFunction? func = new Compiler(input, reporter).Compile();

            Assert.IsNotNull(func);
            if (func == null) {
                throw new NullReferenceException();
            }

            Value result = vm.Interpret(func);

            Assert.AreEqual(toScript + IReporter.NewLine, reporter.Printout);
            Assert.IsNotNull(pong);
            Assert.AreEqual(fromScript, pong);
        }

        [Test]
        public void PreCompiled() {
            SimpleReporter reporter = new SimpleReporter();

            string toScript = string.Empty;
            ObjNative Ping = new ObjNative("ping",
                args => Value.FromString(new ObjString(toScript)),
                0);

            ObjFunction? func = new Compiler("print ping();", reporter).Compile();

            Assert.IsNotNull(func);
            if (func is null) { throw new NullReferenceException(); }

            VM vm = new VM(reporter);
            vm.DefineNative(Ping);

            foreach (string x in new string[] { "first", "second", "third" }) {
                SimpleReporter actual = new SimpleReporter();
                toScript = x;
                vm.Reporter = actual;
                Value v = vm.Interpret(func);
                Assert.AreEqual(toScript + IReporter.NewLine, actual.Printout);
            }
        }

        [Test]
        public void GetFunctions() {
            SimpleReporter reporter = new SimpleReporter();

            ObjFunction? topLevel = new Compiler("fun a() { print 2; } fun b() { print 3; }", reporter).Compile();
            Assert.IsNotNull(topLevel);
            if (topLevel == null) { throw new NullReferenceException(); }

            IList<ObjFunction> funcs = topLevel.GetChildren();

            Assert.AreEqual(2, funcs.Count);
            Assert.AreEqual("a", funcs[0].Name);
            Assert.AreEqual("b", funcs[1].Name);
        }

        [Test]
        public void RunAFunction() {
            SimpleReporter reporter = new SimpleReporter();

            ObjFunction? topLevel = new Compiler("fun subtract(a, b) { return a - b; }", reporter).Compile();

            Assert.IsNotNull(topLevel);
            if (topLevel is null) { throw new NullReferenceException(); }

            ObjFunction? func = topLevel.GetChild("subtract");
            Assert.IsNotNull(func);
            if (func is null) { throw new NullReferenceException(); }

            Value result = func.Call(reporter, Value.FromNumber(3), Value.FromNumber(2));

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsNumber);
            Assert.AreEqual(-1, result.AsNumber);
        }

        [Test]
        public void TopLevelReturn() {
            SimpleReporter reporter = new SimpleReporter();

            ObjFunction? topLevel = new Compiler("return 7;", reporter).Compile();
            Assert.IsNotNull(topLevel);
            if (topLevel == null) { throw new NullReferenceException(); }

            Value actual = new VM(reporter).Interpret(topLevel);

            Assert.IsNotNull(actual);
            Assert.IsTrue(actual.IsNumber);
            Assert.AreEqual(actual.AsNumber, 7);

        }

        [TestCaseSource(nameof(VMCases))]
        public void CompiledCode(string input, string expected) {
            SimpleReporter reporter = new SimpleReporter();

            ObjFunction? func = new Compiler(input, reporter).Compile();

            Assert.IsNotNull(func);
            if (func == null) { throw new NullReferenceException(); }

            VM vm = new VM(reporter);
            _ = vm.Interpret(func);

            Assert.AreEqual(expected + IReporter.NewLine, reporter.Printout);
        }

        public static readonly object[] VMCases = new object[] {
            new object[] { "print 2 + 2;", "4"} ,
            new object[] { "print (2 + 2);", "4"},
            new object[] { "print 3 - 6;", "-3" },
            new object[] { "print 8 / 4;", "2" },
            new object[] { "print 5 * 8;", "40" },
            new object[] { "print 5 == 8;", "false" },
            new object[] { "print 5 != 8;", "true" },
            new object[] { "print 5 > 8;", "false" },
            new object[] { "print 5 >= 8;", "false" },
            new object[] { "print 5 < 8;", "true" },
            new object[] { "print 5 <= 8;", "true" },
            new object[] { "print -8;", "-8" },
            new object[] { "print !8;", "false" },
            new object[] { "print --8;", "8" },
            new object[] { "print true;", "true" },
            new object[] { "print false;", "false" },
            new object[] { "print nil;", "nil" },
            new object[] { "print 3 + 4;", "7" },
            new object[] { "print 7 - 3;", "4" },
            new object[] { "print 5 / 2;", "2.5" },
            new object[] { "print 7 * 7;", "49" },
            new object[] { "print 7 == 7;", "true" },
            new object[] { "print 7 == 8;", "false" },
            new object[] { "print 7 == nil;", "false" },
            new object[] { "print 7 != nil;", "true" },
            new object[] { "print nil == nil;", "true" },
            new object[] { "print true == false;", "false" },
            new object[] { "print false == false;", "true" },
            new object[] { "print 7 == false;", "false" },
            new object[] { "print 1 > 2;", "false" },
            new object[] { "print 1 >= 2;", "false" },
            new object[] { "print 1 < 2;", "true" },
            new object[] { "print 1 <= 2;", "true" },
            new object[] { "print -7;", "-7" },
            new object[] { "print --7;", "7" },
            new object[] { "print false == !true;", "true" },
            new object[] { "print false == !!true;", "false" },
            new object[] { "print \"a\" + \"b\";", "ab" },
            new object[] { "var a = 9; print a;", "9" },
            new object[] { "var b; b = 8; print b;", "8" },
            new object[] { "var a= 4; { var a = 7; print a; }", "7" },
            new object[] { "print true or false;", "true" },
            new object[] { "print true and false;", "false" },
            new object[] { "if (7 >8) print \"error\"; else print \"works\";", "works" },
            new object[] { "if (7 <8) print \"works\"; else print \"error\";", "works" },
            new object[] { "var a = 0; while (a < 3) { a = a + 1; } print a;", "3" },
            new object[] { "var a = 0; for (var i = 0; i < 3; i = i + 1) { a = a +1 ;} print a;", "3" },
            new object[] { "fun a() { print 7; } print a;", "<fn a>" },
            new object[] { "fun a() { return 7; } print a();", "7" },
            new object[] { "fun a() { fun b() { return 6; } return b(); } print a();", "6" },
            new object[] { @"fun outer() { var x = ""outside""; fun inner() { print x; } inner(); } outer();", "outside" },
            new object[] { "class A {} print A;", "A" },
            new object[] { "class A {} print A();", "A Instance" },
            new object[] { "class A {} var b = A(); b.c = 24; print b.c;", "24" },
            new object[] { "class A {} var b = A(); b.c = 24; print b has \"c\";", "true" },
            new object[] { "class A {} var b = A(); b.c = 24; print b has \"d\";", "false" },
            new object[] { "var x = \"c\"; class A {} var b = A(); b.c = 24; print b has x;", "true" },
            new object[] { "var x = \"d\"; class A {} var b = A(); b.c = 24; print b has x;", "false" },
            new object[] { "print \"Hello\".length;", "Hello".Length.ToString() },
            new object[] { "var a= \"Hello\"; print a.length;", "Hello".Length.ToString() },
            new object[] { "print \"Hello\"[0];", "H" },
            new object[] { "var a=\"Hello\"; print a[0];", "H" },
            new object[] { "var a=\"Hello\"; a[0] = \"B\"; print a;", "Bello" },

        };
    }
}
