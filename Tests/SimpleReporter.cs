﻿using System;
using System.IO;

using GameLibrary.Models;
using GameLibrary.Scripting;

namespace Tests {
    public class SimpleReporter : IReporter {

        private readonly TextWriter _stdOut = System.Console.Out;
        private readonly TextWriter _stdErr = System.Console.Error;
        private readonly TextWriter _print = new StringWriter();
        public SimpleReporter() { }
        public void Error(string message) => _stdErr.WriteLine(message);
        public void Error(string message, Exception ex) => _stdErr.WriteLine(message);
        public void Info(string message) => _stdOut.WriteLine(message);
        public void Print(Value value) => _print.WriteLine(value.ToString());

        public string? Printout => _print.ToString();
    }
}
