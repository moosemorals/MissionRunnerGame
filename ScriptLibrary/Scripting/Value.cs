using System;
using System.Collections.Generic;
using System.Linq;

using GameLibrary.Models;

namespace GameLibrary.Scripting {

    public enum ValueType {
        Bool,
        Class,
        Closure,
        Function,
        Indexed,
        Instance,
        Native,
        Nil,
        Number,
        Object,
        String,
    }

    public class Value {
        public ValueType Type { get; init; }
        private double? _number;
        private bool? _bool;
        private object? _object;

        public override string? ToString() {
            switch (Type) {
            case ValueType.Nil:
                return "nil";
            case ValueType.Bool:
                return AsBool ? "true" : "false";
            case ValueType.String:
                return AsString.Str;
            case ValueType.Closure:
                return AsClosure.Function.ToString();
            default:
                return AsObject()?.ToString();
            }
        }

        public object? AsObject() {
            switch (Type) {
            case ValueType.Bool:
                return _bool;
            case ValueType.Number:
                return _number;
            case ValueType.Nil:
                return null;
            default:
                return _object;
            }
        }

        public IPropertied? AsIPropertied() {
            switch (Type) {
            case ValueType.String:
            case ValueType.Instance:
                return _object as IPropertied;
            default:
                return null;
            }
        }

        public bool AsBool => _bool == null ? throw new NullReferenceException() : _bool.Value;
        public bool IsBool => Type == ValueType.Bool;
        public static Value FromBool(bool b) => new Value { Type = ValueType.Bool, _bool = b };

        public ObjClass AsClass => _object as ObjClass ?? throw new NullReferenceException();
        public bool IsClass => Type == ValueType.Class;
        public static Value FromClass(ObjClass c) => new Value { Type = ValueType.Class, _object = c };

        public ObjClosure AsClosure => _object as ObjClosure ?? throw new NullReferenceException();
        public bool IsClosure => Type == ValueType.Closure;
        public static Value FromClosure(ObjClosure c) => new Value { Type = ValueType.Closure, _object = c };

        public ObjFunction AsFunction => _object as ObjFunction ?? throw new NullReferenceException();
        public bool IsFunction => Type == ValueType.Function;
        public static Value FromFunction(ObjFunction f) => new Value { Type = ValueType.Function, _object = f };

        public IPropertied AsInstance => _object as IPropertied ?? throw new NullReferenceException();
        public bool IsInstance => _object is IPropertied;
        public static Value FromInstance(IPropertied f) => new Value { Type = ValueType.Instance, _object = f };

        public IIndexed AsIndexed => _object as IIndexed ?? throw new NullReferenceException();
        public bool IsIndexed => _object is IIndexed;
        public static Value FromIndexed(IIndexed i) => new Value { Type = ValueType.Indexed, _object = i };

        public ObjNative AsNative => _object as ObjNative ?? throw new NullReferenceException();
        public bool IsNative => Type == ValueType.Native;
        public static Value FromNative(ObjNative n) => new Value { Type = ValueType.Native, _object = n };

        public static object? AsNil => null;
        public bool IsNil => Type == ValueType.Nil;
        public static Value FromNil() => new Value { Type = ValueType.Nil };

        public double AsNumber => _number == null ? throw new NullReferenceException() : _number.Value;
        public bool IsNumber => Type == ValueType.Number;
        public static Value FromNumber(double n) => new Value { Type = ValueType.Number, _number = n };

        public ObjString AsString => _object as ObjString ?? throw new NullReferenceException();
        public bool IsString => Type == ValueType.String;
        public static Value FromString(ObjString s) => new Value { Type = ValueType.String, _object = s };

    } 

    public class ObjString : IPropertied, IIndexed {
        public ObjString(string value) {
            Str = value;
        }

        public ObjString(char c) {
            Str = new string(new char[] { c });
        }

        public Value this[Value i] {
            get => i.IsNumber &&  i.AsNumber >= 0 && i.AsNumber <= Str.Length
                ? Value.FromString(new ObjString(Str[(int)i.AsNumber]))
                : Value.FromNil();
            set { 
                if (i.IsNumber &&  i.AsNumber >= 0 && i.AsNumber <= Str.Length
                    && value.IsString) {

                    char[] chars = Str.ToCharArray();

                    chars[(int)i.AsNumber] = value.AsString.Str[0];
                    
                    Str = new string(chars);

                }
            }
        }

        public string Str { get; private set; }

        public Value GetProperty(string name) {
            switch (name) {
            case "length":
                return Value.FromNumber(Str.Length);
            default:
                return Value.FromNil();
            }
        }

        public bool HasProperty(string name) => name == "length";
        public void SetProperty(string name, Value value) {
            // Does nothing 
        }
    }

    public class ObjClass {
        public ObjClass(string name) {
            Name = name;
        }

        public string Name { get; init; }

        public override string ToString() => Name;
    }

    public class ObjClosure {

        public ObjClosure(ObjFunction func) {
            Function = func;
            Upvalues = new ObjUpvalue[func.UpvalueCount];
        }

        public ObjFunction Function { get; init; }

        public ObjUpvalue[] Upvalues { get; init; }

        public int UpvalueCount => Upvalues.Length;
    }

    public class ObjInstance : IPropertied {
        public ObjInstance(ObjClass klass) {
            Klass = klass;
            Fields = new HashTable();
        }

        private ObjClass Klass { get; init; }
        private HashTable Fields { get; init; }

        public Value GetProperty(string name) => Fields[name];
        public bool HasProperty(string name) => Fields.Has(name);
        public void SetProperty(string name, Value value) => Fields[name] = value;
        public override string ToString() => $"{Klass.Name} Instance";
    }

    public class ObjFunction {
        public ObjFunction(string name) {
            Arrity = 0;
            Chunk = new();
            Name = name;
            UpvalueCount = 0;
        }

        public int Arrity { get; set; }
        internal Chunk Chunk { get; set; }
        public string Name { get; set; }
        public int UpvalueCount { get; set; }

        public Value Call(IReporter reporter, params Value[] args) {
            return args.Length != Arrity
                ? throw new RuntimeError($"Expected {Arrity} arguments but got {args.Length}")
                : new VM(reporter).Interpret(this, args);
        }

        public IList<ObjFunction> GetChildren() => Chunk.Functions;

        public ObjFunction? GetChild(string name) => Chunk.Functions.FirstOrDefault(f => f.Name == name);
        public override string ToString() => string.IsNullOrEmpty(Name) ? "<script>" : $"<fn {Name}>";
    }


    public class ObjUpvalue {
        public ObjUpvalue(Value value, int location) {
            Value = value;
            Location = location;
            Next = null;
        }

        public int Location { get; init; }
        public Value Value { get; set; }
        public ObjUpvalue? Next { get; set; }
    }

    public class ObjNative {
        public ObjNative(string name, Func<object?[], Value> function, int argCount) {
            Name=name;
            Function=function;
            ArgCount=argCount;
        }

        public string Name { get; init; }
        public Func<object?[], Value> Function { get; init; }
        public int ArgCount { get; init; }
        public override string ToString() => $"<fn native {Name}>";
    }
}
