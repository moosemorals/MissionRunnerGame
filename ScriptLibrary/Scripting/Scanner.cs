
using static GameLibrary.Scripting.TokenType;

namespace GameLibrary.Scripting {
    internal class Scanner {

        private readonly string _source;
        private int _start;
        private int _current;
        private int _line;

        internal Scanner(string source) {
            _source = source;
            _start = 0;
            _current = 0;
            _line = 1;
        }

        internal Token ScanToken() {
            SkipWhitespace(); 

            _start = _current;

            if (IsAtEnd()) {
                return MakeToken(EOF);
            }

            char c = Advance();
            if (IsAlpha(c)) {
                return Identifier();
            }
            if (IsDigit(c)) {
                return Number();
            }

            switch (c) {
            case '(': return MakeToken(LeftParen);
            case ')': return MakeToken(RightParen);
            case '{': return MakeToken(LeftBrace);
            case '}': return MakeToken(RightBrace);
            case '[': return MakeToken(LeftSquare);
            case ']': return MakeToken(RightSquare);
            case ';': return MakeToken(Semicolon);
            case ',': return MakeToken(Comma);
            case '.': return MakeToken(Dot);
            case '-': return MakeToken(Minus);
            case '+': return MakeToken(Plus);
            case '/': return MakeToken(Slash);
            case '*': return MakeToken(Star);
            case '!': return MakeToken(Match('=') ? BangEqual : Bang);
            case '=': return MakeToken(Match('=') ? EqualEqual : Equal);
            case '<': return MakeToken(Match('=') ? LessEqual : Less);
            case '>': return MakeToken(Match('=') ? GreaterEqual : Greater);
            case '"': return String(); 
            }

            return ErrorToken("Unexpected Character.");
        }

        private char Advance() => _source[_current++];

        private TokenType CheckKeyword(int start, int length, string rest, TokenType type) {
            return _current - _start == start + length
                && rest == _source[( _start + start ).._current]
                    ? type
                    : TokenType.Identifier;
        }

        private Token ErrorToken(string message) => new Token(Error, message, _line);

        private Token Identifier() {
            while (IsAlpha(Peek()) || IsDigit(Peek())) {
                Advance();
            }

            return MakeToken(IdentifierType());
        }

        private TokenType IdentifierType() {
            switch (_source[_start]) {
            case 'a': return CheckKeyword(1, 2, "nd", And);
            case 'c': return CheckKeyword(1, 4, "lass", Class);
            case 'e': return CheckKeyword(1, 3, "lse", Else);
            case 'h': return CheckKeyword(1, 2, "as", Has);
            case 'f':
                if (_current - _start > 1) {
                    switch (_source[_start + 1]) {
                    case 'a': return CheckKeyword(2, 3, "lse", False);
                    case 'o': return CheckKeyword(2, 1, "r", For);
                    case 'u': return CheckKeyword(2, 1, "n", Fun);
                    }
                }
                break;
            case 'i': return CheckKeyword(1, 1, "f", If);
            case 'n': return CheckKeyword(1, 2, "il", Nil);
            case 'o': return CheckKeyword(1, 1, "r", Or);
            case 'p': return CheckKeyword(1, 4, "rint", Print);
            case 'r': return CheckKeyword(1, 5, "eturn", Return);
            case 's': return CheckKeyword(1, 4, "uper", Super);
            case 't':
                if (_current - _start > 1) {
                    switch (_source[_start + 1]) {
                    case 'h': return CheckKeyword(2, 2, "is", This);
                    case 'r': return CheckKeyword(2, 2, "ue", True);
                    }
                }
                break;
            case 'v': return CheckKeyword(1, 2, "ar", Var);
            case 'w': return CheckKeyword(1, 4, "hile", While);

            }


            return TokenType.Identifier;

        }

        private static bool IsAlpha(char c) =>
               ( c >= 'a' && c <= 'z' )
            || ( c >= 'A' && c <= 'Z' )
            || c == '_';

        private bool IsAtEnd() => _current >= _source.Length;

        private static bool IsDigit(char c) => c >= '0' && c <= '9';

        private Token MakeToken(TokenType type) => new Token(type, _source[_start.._current], _line);

        private bool Match(char expected) {
            if (!IsAtEnd() && _source[_current] == expected) {
                _current += 1;
                return true;
            }
            return false;
        }

        private Token Number() {
            while (IsDigit(Peek())) {
                Advance();
            }

            if (Peek() == '.' && IsDigit(PeekNext())) {
                Advance();
                while (IsDigit(Peek())) {
                    Advance();
                }
            }

            return MakeToken(TokenType.Number);
        }

        private char Peek() => IsAtEnd() ? '\0' : _source[_current];

        private char PeekNext() => _current < _source.Length - 1 ? _source[_current + 1] : '\0';

        private void SkipWhitespace() {
            while (true) {
                char c = Peek();
                switch (c) {
                case ' ':
                case '\r':
                case '\t':
                    Advance();
                    break;
                case '\n':
                    _line += 1;
                    Advance();
                    break;
                case '/':
                    if (PeekNext() == '/') {
                        while (!IsAtEnd() && Peek() != '\n') {
                            Advance();
                        }
                    } else {
                        return;
                    }
                    break;
                default:
                    return;
                }
            }
        }

        private Token String() {
            while (!IsAtEnd() && Peek() != '"') {
                if (Peek() == '\n') {
                    _line += 1;
                }
                Advance();
            }

            if (IsAtEnd()) {
                return ErrorToken("Unterminated string.");
            }

            Advance();
            return MakeToken(TokenType.String);

        }

    }
}
