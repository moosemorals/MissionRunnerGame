﻿using System.Collections.Generic;

namespace GameLibrary.Scripting {
    internal class Disassembler {

        internal static string Disassemble(ObjFunction func) {

            string result = "";
            Queue<ObjFunction> q = new();
            q.Enqueue(func);

            while (q.Count > 0) {
                ObjFunction next = q.Dequeue();

                foreach (ObjFunction n in next.Chunk.Functions) {
                    q.Enqueue(n);
                }

                result += DisassembleChunk(next.Chunk, string.IsNullOrEmpty(next.Name) ? "Top Level script" : next.Name);
            }
            return result;
        }

        internal static string DisassembleChunk(Chunk chunk, string name) {
            string result = $"== Start {name} ==\n";

            for (int offset = 0; offset < chunk.Code.Length;) {
                string report;
                (report, offset) = DisassembleInstruction(chunk, offset);
                result += report + "\n";
            }
            result += $"== End {name} ==\n\n";
            return result;
        }

        private static (string, int) DisassembleInstruction(Chunk chunk, int offset) {
            string preamble = $"{offset} {chunk.GetLine(offset)}";

            string report;

            Op instruction = (Op)chunk.Code[offset];
            switch (instruction) {
            case Op.Add:
            case Op.CloseUpvalue:
            case Op.Divide:
            case Op.Equal:
            case Op.False:
            case Op.Greater:
            case Op.Less:
            case Op.Multiply:
            case Op.Negate:
            case Op.Nil:
            case Op.Not:
            case Op.Pop:
            case Op.Print:
            case Op.Return:
            case Op.Subtract:
            case Op.True:
                (report, offset) = SimpleInstruction(chunk, instruction, offset);
                break;
            case Op.Class:
            case Op.Constant:
            case Op.DefineGlobal:
            case Op.GetGlobal:
            case Op.GetProperty:
            case Op.SetGlobal:
            case Op.SetProperty:
                (report, offset) = ConstantInstruction(chunk, instruction, offset);
                break;
            case Op.Call:
            case Op.GetLocal:
            case Op.GetUpvalue:
            case Op.SetLocal:
            case Op.SetUpvalue:
                (report, offset) = ByteInstruction(chunk, instruction, offset);
                break;
            case Op.Jump:
            case Op.JumpIfFalse:
                (report, offset) = JumpInstruction(chunk, instruction, 1, offset);
                break;
            case Op.Loop:
                (report, offset) = JumpInstruction(chunk, instruction, -1, offset);
                break;
            case Op.Closure: {
                    offset += 1;
                    byte constant = chunk.Code[offset++];
                    report = $"{instruction} {constant} {chunk.Constants[constant]}\n";

                    ObjFunction func = chunk.Constants[constant].AsFunction;

                    for (int j = 0; j < func.UpvalueCount; j += 1) {
                        bool isLocal = chunk.Code[offset++] == 1;
                        int index = chunk.Code[offset++];
                        report += $"   {offset - 2} {( isLocal ? "local" : "upvalue" )} {index}\n";
                    }
                    break;

                }
            default:
                report = $"Unknown opcode {instruction}";
                offset += 1;
                break;
            }

            return ($"{preamble} {report}", offset);
        }

        private static (string, int) ByteInstruction(Chunk chunk, Op instruction, int offset) {
            byte slot = chunk.Code[offset + 1];
            return ($"{instruction} {slot}", offset + 2);
        }

        private static (string, int) ConstantInstruction(Chunk chunk, Op instruction, int offset) {
            byte constant = chunk.Code[offset + 1];
            return ($"{instruction} {constant} '{chunk.Constants[constant]}'", offset + 2);
        }

        private static (string, int) JumpInstruction(Chunk chunk, Op instruction, int sign, int offset) {
            ushort jump = (ushort)( chunk.Code[offset + 1] << 8 | chunk.Code[offset + 2] );

            return ($"{instruction} {offset + 3 + sign * jump}", offset + 3);
        }

        private static (string, int) SimpleInstruction(Chunk chunk, Op instruction, int offset) {
            return ($"{instruction}", offset + 1);
        }

    }
}
