using System.Collections.Generic;

namespace GameLibrary.Scripting {
    internal class HashTable {

        private readonly Dictionary<string, Value> _values = new();

        internal Value this[string key] {
            get {
                return _values.ContainsKey(key)
                    ? _values[key]
                    : Value.FromNil();
            }
            set {
                if (_values.ContainsKey(key)) {
                    _values[key] = value;
                } else {
                    _values.Add(key, value);
                }
            }
        }

        internal bool Has(string key) => _values.ContainsKey(key);
    }
}
