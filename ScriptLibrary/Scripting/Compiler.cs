using System;
using System.Linq;

using GameLibrary.Models;

using static GameLibrary.Scripting.TokenType;

namespace GameLibrary.Scripting {

    public class Compiler {

        private const int VariableNotFound = -1;

        private CompilerState _state;
        private Token _currentToken;
        private Token _previousToken;
        private bool _hadError = false;
        private bool _panicMode = false;
        private readonly ParseRule[] _rules;
        private readonly Scanner _scanner;
        private readonly IReporter _reporter;

        public Compiler(string source, IReporter reporter) {
            _scanner = new Scanner(source);
            _reporter = reporter;

            _state = new CompilerState(null, new ObjFunction(""), FunctionType.Script);

            _previousToken = new Token(EOF, "", 0);
            _currentToken = new Token(EOF, "", 0);

            _rules = new[] {
                new ParseRule(Bang, Unary, null, Precedence.None),
                new ParseRule(BangEqual, null, Binary, Precedence.Equality),
                new ParseRule(Class, null, null, Precedence.None),
                new ParseRule(Comma, null, null, Precedence.None),
                new ParseRule(EOF, null, null, Precedence.None),
                new ParseRule(Else, null, null, Precedence.None),
                new ParseRule(Equal,null, null, Precedence.None),
                new ParseRule(EqualEqual, null, Binary, Precedence.Equality),
                new ParseRule(False, Literal, null, Precedence.None),
                new ParseRule(For, null, null, Precedence.None),
                new ParseRule(Fun, null, null, Precedence.None),
                new ParseRule(Greater, null, Binary, Precedence.Comparison),
                new ParseRule(GreaterEqual, null, Binary, Precedence.Comparison),
                new ParseRule(Has, null, Binary, Precedence.Comparison),
                new ParseRule(Identifier, Variable, null, Precedence.None),
                new ParseRule(If, null, null, Precedence.None),
                new ParseRule(LeftBrace, null, null, Precedence.None),
                new ParseRule(LeftParen, Grouping, Call, Precedence.Call),
                new ParseRule(LeftSquare,null, Index, Precedence.Call),
                new ParseRule(Less, null, Binary, Precedence.Comparison),
                new ParseRule(LessEqual, null, Binary, Precedence.Comparison),
                new ParseRule(Minus, Unary,Binary, Precedence.Term),
                new ParseRule(Nil, Literal, null, Precedence.None),
                new ParseRule(Plus, null, Binary, Precedence.Term),
                new ParseRule(Print, null, null, Precedence.None),
                new ParseRule(Return, null, null, Precedence.None),
                new ParseRule(RightBrace, null, null, Precedence.None),
                new ParseRule(RightParen, null, null, Precedence.None),
                new ParseRule(RightSquare,null, null, Precedence.None),
                new ParseRule(Semicolon, null, null, Precedence.None),
                new ParseRule(Slash,null, Binary, Precedence.Factor),
                new ParseRule(Star, null, Binary, Precedence.Factor),
                new ParseRule(Super, null, null, Precedence.None),
                new ParseRule(This, null, null, Precedence.None),
                new ParseRule(TokenType.And, null, And, Precedence.And),
                new ParseRule(TokenType.Dot, null, Dot, Precedence.Call),
                new ParseRule(TokenType.Error, null, null, Precedence.None),
                new ParseRule(TokenType.Number, Number, null, Precedence.None),
                new ParseRule(TokenType.Or, null, Or, Precedence.Or),
                new ParseRule(TokenType.String, String, null, Precedence.None),
                new ParseRule(True, Literal, null, Precedence.None),
                new ParseRule(Var, null, null, Precedence.None),
                new ParseRule(While, null, null, Precedence.None),
            }.OrderBy(p => p.Type).ToArray();
        }

        public ObjFunction? Compile() {
            Advance();

            while (!Match(EOF)) {
                Declaration();
            }

            ObjFunction func = EndCompiler();

            return _hadError ? null : func;
        }

        private void AddLocal(string name) {
            if (_state.LocalCount == byte.MaxValue) {
                Error("Too many local variables in function.");
                return;
            }

            _state.Locals[_state.LocalCount++] = new Local(name, -1);
        }

        private int AddUpvalue(CompilerState state, byte index, bool isLocal) {
            int upvalueCount = state.Function.UpvalueCount;

            for (int i = 0; i < upvalueCount; i += 1) {
                Upvalue upvalue = state.Upvalues[i];
                if (upvalue.Index == index && upvalue.IsLocal == isLocal) {
                    return i;
                }
            }

            if (upvalueCount == byte.MaxValue) {
                Error("Too many closure variables in function.");
                return 0;
            }

            state.Upvalues[upvalueCount] = new Upvalue(index, isLocal);
            return state.Function.UpvalueCount++;
        }

        private void Advance() {
            _previousToken = _currentToken;

            while (true) {
                _currentToken = _scanner.ScanToken();
                if (_currentToken.Type != TokenType.Error) {
                    break;
                }

                ErrorAtCurrent(_currentToken.Lexeme);
            }
        }

        private void And(bool canAssgn) {
            int endJump = EmitJump(Op.JumpIfFalse);

            EmitByte(Op.Pop);
            ParsePrecedence(Precedence.And);
            PatchJump(endJump);
        }

        private byte ArgumentList() {
            byte argCount = 0;
            if (!Check(RightParen)) {
                do {
                    Expression();
                    if (argCount == 255) {
                        Error("Can't have more than 255 arguments");
                    }
                    argCount += 1;
                } while (Match(Comma));
            }

            Consume(RightParen, "Expect ')' after arguments.");
            return argCount;
        }

        private void BeginScope() {
            _state.Depth += 1;
        }

        private void Binary(bool canAssign) {
            TokenType opType = _previousToken.Type;

            ParseRule rule = GetRule(opType);
            ParsePrecedence(rule.Precedence + 1);

            switch (opType) {
            case Plus: EmitByte(Op.Add); break;
            case Minus: EmitByte(Op.Subtract); break;
            case Star: EmitByte(Op.Multiply); break;
            case Slash: EmitByte(Op.Divide); break;
            case EqualEqual: EmitByte(Op.Equal); break;
            case BangEqual: EmitBytes(Op.Equal, (byte)Op.Not); break;
            case Greater: EmitByte(Op.Greater); break;
            case GreaterEqual: EmitBytes(Op.Less, (byte)Op.Not); break;
            case Less: EmitByte(Op.Less); break;
            case LessEqual: EmitBytes(Op.Greater, (byte)Op.Not); break;
            case Has: EmitByte(Op.Has); break;
            default:
                throw new Exception("Unreachable code reached");
            }
        }

        private void BlockStatemnt() {
            while (!Check(RightBrace) && !Check(EOF)) {
                Declaration();
            }

            Consume(RightBrace, "Expected '}' after block.");
        }

        private void Call(bool canAssign) {
            byte argCount = ArgumentList();
            EmitBytes(Op.Call, argCount);
        }

        private bool Check(TokenType type) {
            return _currentToken.Type == type;
        }

        private void ClassDeclaration() {
            Consume(TokenType.Identifier, "Expected class name.");
            byte nameConstant = IdentifierConstant(_previousToken.Lexeme);
            DeclareVariable();

            EmitBytes(Op.Class, nameConstant);
            DefineVariable(nameConstant);

            Consume(LeftBrace, "Expected '{' before class body.");
            Consume(RightBrace, "Expected '}' before class body.");
        }

        private void Consume(TokenType type, string message) {
            if (_currentToken.Type == type) {
                Advance();
                return;
            }

            ErrorAtCurrent(message);
        }

        private Chunk CurrentChunk() {
            return _state.Function.Chunk;
        }

        private void Declaration() {
            if (Match(Class)) {
                ClassDeclaration();
            } else if (Match(Fun)) {
                FunDeclaration();
            } else if (Match(Var)) {
                VarDeclaration();
            } else {
                Statement();
            }

            if (_panicMode) {
                Synchronize();
            }
        }

        private void DeclareVariable() {
            if (_state.Depth == 0) {
                return;
            }

            string name = _previousToken.Lexeme;
            for (int i = _state.LocalCount - 1; i >= 0; i -= 1) {
                Local local = _state.Locals[i];
                if (local.Depth != -1 && local.Depth < _state.Depth) {
                    break;
                }

                if (name.Equals(local.Name)) {
                    Error($"Already variable named {name} in this scope.");
                }
            }
            AddLocal(name);
        }

        private void DefineVariable(byte global) {
            if (_state.Depth > 0) {
                MarkInitialized();
                return;
            }

            EmitBytes(Op.DefineGlobal, global);
        }

        private void Dot(bool canAssign) {
            Consume(Identifier, "Expected property name after '.'.");
            byte name = IdentifierConstant(_previousToken.Lexeme);

            if (canAssign && Match(Equal)) {
                Expression();
                EmitBytes(Op.SetProperty, name);
            } else {
                EmitBytes(Op.GetProperty, name);
            }
        }

        private void ErrorAtCurrent(string message) {
            ErrorAt(_currentToken, message);
        }

        private void Error(string message) {
            ErrorAt(_previousToken, message);
        }

        private void ErrorAt(Token token, string message) {
            if (_panicMode) {
                return;
            }
            _panicMode = true;
            string location = $"[line {token.Line}] Error";

            if (token.Type == EOF) {
                location += " at end";
            } else if (token.Type == TokenType.Error) {
                // Skipped
            } else {
                location += $" at '{token.Lexeme}'";
            }

            _reporter.Error($"{location}: {message}");
            _hadError = true;
        }

        private void EmitByte(byte b) {
            CurrentChunk().Add(b, _previousToken.Line);
        }

        private void EmitByte(Op code) {
            CurrentChunk().Add((byte)code, _previousToken.Line);
        }

        private void EmitBytes(Op code, byte b2) {
            EmitByte(code);
            EmitByte(b2);
        }

        private void EmitConstant(Value value) {
            EmitBytes(Op.Constant, MakeConstant(value));
        }

        private int EmitJump(Op instruction) {
            EmitByte(instruction);
            EmitByte(0xff);
            EmitByte(0xff);
            return CurrentChunk().Count - 2;
        }

        private void EmitLoop(int loopStart) {
            EmitByte(Op.Loop);

            int offset = CurrentChunk().Count - loopStart + 2;
            if (offset > ushort.MaxValue) {
                Error("Loop body too large.");
            }

            EmitByte((byte)( ( offset >> 8 ) & 0xff ));
            EmitByte((byte)( offset & 0xff ));
        }

        private void EmitReturn() {
            EmitByte(Op.Nil);
            EmitByte(Op.Return);
        }

        private ObjFunction EndCompiler() {
            // If the function doesn't end with a return, add one
            if (CurrentChunk().Code[CurrentChunk().Count - 1] != (byte)Op.Return) {
                EmitReturn();
            }

            ObjFunction func = _state.Function;

            if (_state.Enclosing != null) {
                _state = _state.Enclosing;
            }

            return func;
        }

        private void EndScope() {
            _state.Depth -= 1;

            // Clean the stack of all in-scope locals
            while (_state.LocalCount > 0 && _state.Locals[_state.LocalCount - 1].Depth > _state.Depth) {
                if (_state.Locals[_state.LocalCount - 1].IsCaptured) {
                    EmitByte(Op.CloseUpvalue);
                } else {
                    EmitByte(Op.Pop);
                }
                _state.LocalCount -= 1;
            }
        }

        private void Expression() {
            ParsePrecedence(Precedence.Assignment);
        }

        private void ExpressionStatement() {
            Expression();
            Consume(Semicolon, "Expected ';' after expression.");
            EmitByte(Op.Pop);
        }

        private void ForStatement() {
            BeginScope();

            Consume(LeftParen, "Expected '(' after 'for'.");

            // Initializer
            if (Match(Semicolon)) {
                // No initializer
            } else if (Match(Var)) {
                VarDeclaration();
            } else {
                ExpressionStatement();
            }

            int loopStart = CurrentChunk().Count;

            // Condition
            int exitJump = -1;
            if (!Match(Semicolon)) {
                Expression();
                Consume(Semicolon, "Expect ';' after loop condition.");

                exitJump = EmitJump(Op.JumpIfFalse);
                EmitByte(Op.Pop);
            }

            // Increment
            if (!Match(RightParen)) {
                // Jump over the increment code
                int bodyJump = EmitJump(Op.Jump);

                int incrementStart = CurrentChunk().Count;
                Expression();
                EmitByte(Op.Pop);
                Consume(RightParen, "Expected ')' after for clauses.");

                EmitLoop(loopStart);
                loopStart = incrementStart;
                PatchJump(bodyJump);
            }

            Statement();

            // Jump to the top of the body,
            // or to the increment (if we've got one)
            EmitLoop(loopStart);

            // Patch up condition jump (if we've got one)
            if (exitJump != -1) {
                PatchJump(exitJump);
                EmitByte(Op.Pop);
            }

            EndScope();
        }

        private void Function(FunctionType type) {
            CompilerState state = new CompilerState(_state, new ObjFunction(_previousToken.Lexeme), type);
            _state = state;
            BeginScope();

            // Read parameters and turn them into a bunch
            // of local variables
            Consume(LeftParen, "Expected '(' after function name.");
            if (!Check(RightParen)) {
                do {
                    _state.Function.Arrity += 1;
                    if (_state.Function.Arrity > 255) {
                        ErrorAtCurrent("Can't have more than 255 parameters.");
                    }

                    byte param = ParseVariable("Expected parameter name");
                    DefineVariable(param);
                } while (Match(Comma));
            }
            Consume(RightParen, "Expeced ')' after function parameters.");

            // Read the body of the function
            Consume(LeftBrace, "Expected '{' before function body.");
            BlockStatemnt();

            ObjFunction func = EndCompiler();
            EmitBytes(Op.Closure, MakeConstant(Value.FromFunction(func)));

            for (int i = 0; i < func.UpvalueCount; i += 1) {
                EmitByte(state.Upvalues[i].IsLocal ? 1 : 0);
                EmitByte(state.Upvalues[i].Index);
            }
        }

        private void FunDeclaration() {
            byte global = ParseVariable("Expected function name.");
            MarkInitialized();
            Function(FunctionType.Function);
            DefineVariable(global);

        }

        private byte IdentifierConstant(string name) => MakeConstant(Value.FromString(new ObjString(name)));

        private void IfStatement() {
            Consume(LeftParen, "Expected '(' after 'if'.");
            Expression();
            Consume(RightParen, "Expected ')' after 'if' condition.");

            int thenJump = EmitJump(Op.JumpIfFalse);
            EmitByte(Op.Pop);
            Statement();

            int elseJump = EmitJump(Op.Jump);
            PatchJump(thenJump);
            EmitByte(Op.Pop);
            if (Match(Else)) {
                Statement();
            }
            PatchJump(elseJump);
        }


        private void Index(bool canAssign) {
            Expression();
            Consume(RightSquare, "Expected ']' after index expression.");

            if (canAssign && Match(Equal)) {
                Expression();
                EmitByte(Op.SetIndex);
            } else {
                EmitByte(Op.GetIndex);
            }
        }

        private ParseRule GetRule(TokenType type) {
            return _rules[(int)type];
        }

        private void Grouping(bool canAssign) {
            Expression();
            Consume(RightParen, "Expected ')' after expression.");
        }

        private void Literal(bool canAssign) {
            switch (_previousToken.Type) {
            case False: EmitByte(Op.False); break;
            case True: EmitByte(Op.True); break;
            case Nil: EmitByte(Op.Nil); break;
            default:
                throw new Exception("Unreachable code reached");
            }
        }

        private byte MakeConstant(Value value) {
            int constant = CurrentChunk().AddConstant(value);
            if (constant > byte.MaxValue) {
                Error("Too many constants in one chunk.");
                return 0;
            }

            return (byte)constant;
        }

        private void MarkInitialized() {
            if (_state.Depth == 0) {
                return;
            }
            _state.Locals[_state.LocalCount - 1].Depth =
                _state.Depth;
        }

        private bool Match(TokenType type) {
            if (!Check(type)) {
                return false;
            }
            Advance();
            return true;
        }

        private void NamedVariable(string name, bool canAssign) {
            Op getOp, setOp;

            int arg = ResolveLocal(_state, name);
            if (arg != VariableNotFound) {
                getOp = Op.GetLocal;
                setOp = Op.SetLocal;
            } else if (( arg = ResolveUpvalue(_state, name) ) != VariableNotFound) {
                getOp = Op.GetUpvalue;
                setOp = Op.SetUpvalue;
            } else {
                arg = IdentifierConstant(name);
                getOp = Op.GetGlobal;
                setOp = Op.SetGlobal;
            }

            if (canAssign && Match(Equal)) {
                Expression();
                EmitBytes(setOp, (byte)arg);
            } else {
                EmitBytes(getOp, (byte)arg);
            }
        }

        private void Number(bool canAssign) {
            EmitConstant(Value.FromNumber(double.Parse(_previousToken.Lexeme)));
        }

        private void Or(bool canAssign) {
            int elseJump = EmitJump(Op.JumpIfFalse);
            int endJump = EmitJump(Op.Jump);

            PatchJump(elseJump);
            EmitByte(Op.Pop);
            ParsePrecedence(Precedence.Or);
            PatchJump(endJump);
        }

        private void ParsePrecedence(Precedence precedence) {
            Advance();
            Action<bool>? prefixRule = GetRule(_previousToken.Type).Prefix;
            if (prefixRule == null) {
                Error("Expected expression");
                return;
            }

            bool canAssign = precedence <= Precedence.Assignment;
            prefixRule(canAssign);

            while (precedence <= GetRule(_currentToken.Type).Precedence) {
                Advance();
                Action<bool>? infixRule = GetRule(_previousToken.Type).Infix;
                if (infixRule == null) {
                    Error("Expected expression");
                    return;
                }
                infixRule(canAssign);
            }

            if (canAssign && Match(Equal)) {
                Error("Invalid assignment target.");
            }

        }

        private byte ParseVariable(string errorMessage) {
            Consume(Identifier, errorMessage);

            DeclareVariable();
            return _state.Depth > 0 ? 0 : IdentifierConstant(_previousToken.Lexeme);
        }

        private void PatchJump(int offset) {
            int jump = CurrentChunk().Count - offset - 2;

            if (jump > ushort.MaxValue) {
                Error("Too much code to jump over.");
            }

            CurrentChunk().Code[offset] = (byte)( ( jump >> 8 ) & 0xff );
            CurrentChunk().Code[offset + 1] = (byte)( jump & 0xff );
        }

        private void PrintStatement() {
            Expression();
            Consume(Semicolon, "Expected ';' after value.");
            EmitByte(Op.Print);
        }

        private int ResolveLocal(CompilerState state, string name) {
            for (int i = state.LocalCount - 1; i >= 0; i -= 1) {
                Local local = state.Locals[i];
                if (name.Equals(local.Name)) {
                    if (local.Depth == -1) {
                        Error("Can't read local variable in it's own initializer.");
                    }
                    return i;
                }
            }
            return VariableNotFound;
        }

        private int ResolveUpvalue(CompilerState state, string name) {
            if (state.Enclosing == null) {
                return VariableNotFound;
            }

            int local = ResolveLocal(state.Enclosing, name);
            if (local != VariableNotFound) {
                state.Enclosing.Locals[local].IsCaptured = true;
                return AddUpvalue(state, (byte)local, true);
            }

            int upvalue = ResolveUpvalue(state.Enclosing, name);
            if (upvalue != VariableNotFound) {
                return AddUpvalue(state, (byte)upvalue, false);
            }

            return VariableNotFound;
        }

        private void ReturnStatement() {

            if (Match(Semicolon)) {
                EmitReturn();
            } else {
                Expression();
                Consume(Semicolon, "Expect ';' after return value.");
                EmitByte(Op.Return);
            }
        }

        private void Statement() {
            if (Match(Print)) {
                PrintStatement();
            } else if (Match(For)) {
                ForStatement();
            } else if (Match(If)) {
                IfStatement();
            } else if (Match(While)) {
                WhileStatement();
            } else if (Match(Return)) {
                ReturnStatement();
            } else if (Match(LeftBrace)) {
                BeginScope();
                BlockStatemnt();
                EndScope();
            } else {
                ExpressionStatement();
            }
        }

        private void String(bool canAssign) {
            // Remember to strip quotes from lexeme
            EmitConstant(Value.FromString(new ObjString(_previousToken.Lexeme[1..^1])));
        }

        private void Synchronize() {
            _panicMode = false;

            while (_currentToken.Type != EOF) {
                if (_previousToken.Type == Semicolon) {
                    return;
                }

                switch (_currentToken.Type) {
                case Class:
                case For:
                case Fun:
                case If:
                case Print:
                case Return:
                case Var:
                case While:
                    return;
                default:
                    // Nothing
                    break;

                }

                Advance();
            }
        }

        private void Unary(bool canAssign) {
            TokenType opType = _previousToken.Type;

            ParsePrecedence(Precedence.Unary);

            switch (opType) {
            case Bang: EmitByte(Op.Not); break;
            case Minus: EmitByte(Op.Negate); break;
            default:
                throw new Exception("Unreachable code reached");

            }
        }

        private void VarDeclaration() {
            byte global = ParseVariable("Expected variable name.");

            if (Match(Equal)) {
                Expression();
            } else {
                EmitByte(Op.Nil);
            }
            Consume(Semicolon, "Expected ';' after variable declaration.");

            DefineVariable(global);
        }

        private void Variable(bool canAssign) {
            NamedVariable(_previousToken.Lexeme, canAssign);
        }

        private void WhileStatement() {
            int loopStart = CurrentChunk().Count;

            Consume(LeftParen, "Expected '(' after 'while'.");
            Expression();
            Consume(RightParen, "Expected ')' after while condition.");

            int exitJump = EmitJump(Op.JumpIfFalse);
            EmitByte(Op.Pop);
            Statement();
            EmitLoop(loopStart);

            PatchJump(exitJump);
            EmitByte(Op.Pop);

        }

        class CompilerState {
            public CompilerState(CompilerState? enclosing, ObjFunction function, FunctionType type) {
                Function = function;
                Type = type;
                Enclosing = enclosing;
                Upvalues = new Upvalue[byte.MaxValue];
                Locals = new Local[byte.MaxValue];
                Locals[LocalCount++] = new Local("", 0);
            }

            public ObjFunction Function { get; set; }
            public FunctionType Type { get; set; }
            public CompilerState? Enclosing { get; init; }
            public Local[] Locals { get; init; }
            public Upvalue[] Upvalues { get; init; }
            public int Depth { get; set; }
            public int LocalCount { get; set; }
        }

        private class Local {
            public Local(string name, int depth) {
                Name = name;
                Depth = depth;
                IsCaptured = false;
            }

            public string Name { get; init; }
            public int Depth { get; set; }
            public bool IsCaptured { get; set; }
        }

        private record ParseRule(TokenType Type, Action<bool>? Prefix, Action<bool>? Infix, Precedence Precedence);

        private record Upvalue(byte Index, bool IsLocal);

        private enum Precedence {
            None,
            Assignment,
            Or,
            And,
            Equality,
            Comparison,
            Term,
            Factor,
            Unary,
            Call,
            Primary,
        }

        public enum FunctionType {
            Function,
            Script,
        }
    }

}
