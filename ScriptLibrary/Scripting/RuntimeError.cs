using System;

namespace GameLibrary.Scripting {
    public class RuntimeError : Exception {

        public RuntimeError(string message) : base(message) { }
    }
}
