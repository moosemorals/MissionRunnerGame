using System.Collections.Generic;
using System.Linq;

namespace GameLibrary.Scripting {
    internal class Chunk {

        private readonly DynamicArray<int> _lines;
        internal DynamicArray<byte> Code { get; init; }
        internal DynamicArray<Value> Constants { get; init; }

        internal Chunk() {
            Code = new();
            _lines = new();
            Constants = new();

        }
        internal int AddConstant(Value value) => Constants.Push(value);

        internal void Add(byte b, int line) {
            Code.Push(b);
            _lines.Push(line);
        }

        internal int GetLine(int offset) => _lines[offset];

        internal int Count => Code.Length;

        internal IList<ObjFunction> Functions => Constants
            .Where(v => v.IsFunction)
            .Select(v => v.AsFunction)
            .ToList();

    }
}
