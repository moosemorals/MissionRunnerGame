namespace GameLibrary.Scripting {
    public record Token(TokenType Type, string Lexeme, int Line);

    public enum TokenType {
 
        And,
        Bang,
        BangEqual,
        Class,
        Dot,
        EOF,
        Else,
        Equal,
        EqualEqual,
        Error,
        False,
        For,
        Fun,
        Greater,
        GreaterEqual,
        Has,
        Identifier,
        If,
        LeftBrace,
        LeftParen,
        LeftSquare,
        Less,
        LessEqual,
        Minus,
        Nil,
        Number,
        Or,
        Plus,
        Print,
        Return,
        RightBrace,
        RightParen,
        RightSquare,
        Semicolon,
        Slash,
        Star,
        String,
        Super,
        This,
        True,
        Var,
        While,
       Comma,
    }
}
