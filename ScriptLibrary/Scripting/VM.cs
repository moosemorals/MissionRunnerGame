using System;
using System.Linq;

using GameLibrary.Models;

namespace GameLibrary.Scripting {

    public class VM {

        private const int FramesMax = 64;
        private const int StackMax = FramesMax * byte.MaxValue;

        private readonly HashTable _globals;
        private readonly CallFrame[] _frames;
        private readonly Value[] _stack;

        private int _frameCount;
        private int _stackTop;
        private ObjUpvalue? _openUpvalues = null;

        public IReporter Reporter { get; set; }
        public bool Debug { get; set; }

        public VM(IReporter reporter) {
            _frames = new CallFrame[FramesMax];
            _globals = new();
            _stack = new Value[StackMax];
            Reporter = reporter;
        }

        public Value Interpret(ObjFunction func, params Value[] args) {

            Push(Value.FromClosure(new ObjClosure(func)));
            foreach (Value arg in args.Reverse()) {
                Push(arg);
            }

            CallClosure(Peek(args.Length).AsClosure, args.Length);

            return Run();
        }

        public void DefineNative(ObjNative native) {
            AddGlobal(native.Name, Value.FromNative(native));
        }

        public void AddGlobal(string name, Value value) {
            _globals[name] = value;
        }

        public void AddGlobal(string name, double value) {
            _globals[name] = Value.FromNumber(value);
        }

        private Value Run() {

            CallFrame frame = _frames[_frameCount - 1];

            byte ReadByte() => frame.Code[frame.Ip++];

            Value ReadConstant() => frame.Constants[ReadByte()];

            ushort ReadShort() {
                frame.Ip += 2;
                return (ushort)( ( frame.Code[frame.Ip - 2] << 8 | frame.Code[frame.Ip - 1] ) );
            }

            string ReadString() => ReadConstant().AsString.Str;

            while (true) {
                Op instruction = (Op)ReadByte();
                switch (instruction) {

                case Op.Add: {
                    if (Peek(0).IsString && Peek(1).IsString) {
                        string right = Pop().AsString.Str;
                        string left = Pop().AsString.Str;
                        Push(Value.FromString(new ObjString(left + right)));
                    } else if (Peek(0).IsNumber && Peek(1).IsNumber) {
                        double right = Pop().AsNumber;
                        double left = Pop().AsNumber;
                        Push(Value.FromNumber(left + right));
                    } else {
                        throw RuntimeError("Operands to plus must be two numbers or two strings");
                    }

                    break;
                }
                case Op.Call: {
                    int argCount = ReadByte();
                    CallValue(Peek(argCount), argCount);
                    frame = _frames[_frameCount - 1];
                    break;
                }
                case Op.Class: Push(Value.FromClass(new ObjClass(ReadString()))); break;
                case Op.CloseUpvalue:
                    CloseUpvalues(_stackTop - 1);
                    Pop();
                    break;
                case Op.Closure: {
                    ObjFunction func = ReadConstant().AsFunction;
                    ObjClosure closure = new ObjClosure(func);
                    Push(Value.FromClosure(closure));

                    for (int i = 0; i < closure.UpvalueCount; i += 1) {
                        bool isLocal = ReadByte() == 1;
                        byte index = ReadByte();
                        if (isLocal) {
                            closure.Upvalues[i] = CaptureUpvalue(frame.Slots[index], index + frame.Slots.Offset);
                        } else {
                            closure.Upvalues[i] = frame.Closure.Upvalues[index];
                        }
                    }

                    break;
                }
                case Op.Constant: {
                    Value constant = ReadConstant();
                    Push(constant);
                    break;
                }
                case Op.DefineGlobal: {
                    string name = ReadString();
                    _globals[name] = Peek(0);
                    Pop();
                    break;
                }
                case Op.Divide: BinaryOp(Op.Divide); break; 
                case Op.Equal: {
                    Value b = Pop();
                    Value a = Pop();
                    Push(Value.FromBool(ValuesEqual(a, b)));
                    break;
                }
                case Op.False: Push(Value.FromBool(false)); break; 
                case Op.GetGlobal: {
                    string name = ReadString();
                    if (!_globals.Has(name)) {
                        throw RuntimeError($"Undefined variable {name}.");
                    }
                    Push(_globals[name]);
                    break;
                }
                case Op.GetLocal: {
                    byte slot = ReadByte();
                    Push(frame.Slots[slot]);
                    break;
                }
                case Op.GetProperty: {
                    if (!Peek(0).IsInstance) {
                        throw RuntimeError($"Only instances have properties");
                    }

                    IPropertied instance = Peek(0).AsInstance;
                    string name = ReadString();

                    if (instance.HasProperty(name)) {
                        Pop(); // instance
                        Push(instance.GetProperty(name));
                        break;
                    }
                    throw new RuntimeError($"Undefined property {name}.");
                }
                case Op.GetUpvalue: {
                    byte slot = ReadByte();
                    Push(frame.Closure.Upvalues[slot].Value);
                    break;
                }
                case Op.Greater: BinaryOp(Op.Greater); break;
                case Op.Has: {
                    Value name = Pop();
                    Value instance = Pop();

                    if (!name.IsString) {
                        throw RuntimeError($"Can only lookup properties with strings.");
                    }

                    IPropertied? p = instance.AsIPropertied();
                    if (p != null) {
                        Push(Value.FromBool(p.HasProperty(name.AsString.Str)));
                        break;
                    }
                    throw RuntimeError($"Only instances have properties.");

                }
                case Op.Jump: {
                    ushort offset = ReadShort();
                    frame.Ip += offset;
                    break;
                }
                case Op.JumpIfFalse: {
                    ushort offset = ReadShort();
                    if (IsFalsy(Peek(0))) {
                        frame.Ip += offset;
                    }
                    break;
                }
                case Op.Less: BinaryOp(Op.Less); break;
                case Op.Loop: {
                    ushort offset = ReadShort();
                    frame.Ip -= offset;
                    break;
                }
                case Op.Multiply: BinaryOp(Op.Multiply); break;
                case Op.Negate:
                    if (!Peek(0).IsNumber) {
                        throw RuntimeError("Operand to '-' must be a number");
                    }
                    Push(Value.FromNumber(-Pop().AsNumber));
                    break;
                case Op.Nil: Push(Value.FromNil()); break;
                case Op.Not: Push(Value.FromBool(IsFalsy(Pop()))); break;
                case Op.Pop: Pop(); break;
                case Op.Print: Reporter.Print(Pop()); break;
                case Op.Return: {
                    Value result = Pop();
                    CloseUpvalues(frame.Offset);
                    _frameCount--;
                    if (_frameCount == 0) {
                        return result;
                    }

                    _stackTop = frame.Offset;
                    Push(result);

                    frame = _frames[_frameCount - 1];
                    break;
                }
                case Op.SetGlobal: {
                    string name = ReadString();
                    if (!_globals.Has(name)) {
                        throw RuntimeError($"Undefined variable {name}.");
                    }
                    _globals[name] = Peek(0);
                    break;
                }
                case Op.SetLocal: {
                    byte slot = ReadByte();
                    frame.Slots[slot] = Peek(0);
                    break;
                }
                case Op.SetProperty: {
                    if (!Peek(1).IsInstance) {
                        throw RuntimeError($"Only instances have properties");
                    }

                    IPropertied instance = Peek(1).AsInstance;
                    instance.SetProperty(ReadString(), Peek(0));

                    Value value = Pop();
                    Pop(); // instance
                    Push(value);
                    break;
                }
                case Op.SetUpvalue: {
                    byte slot = ReadByte();
                    frame.Closure.Upvalues[slot].Value = Peek(0);
                    break;
                }
                case Op.Subtract: BinaryOp(Op.Subtract); break;
                case Op.True: Push(Value.FromBool(true)); break;
                case Op.GetIndex: {

                    if (!Peek(1).IsIndexed) {
                        throw RuntimeError($"Only indexed objects have indexes");
                    }

                    Value index = Pop();
                    IIndexed indexed = Pop().AsIndexed;

                    Push(indexed[index]);

                    break;
                }
                case Op.SetIndex: {

                    if (!Peek(2).IsIndexed) {
                        throw RuntimeError("Only indexed objects have indexes");
                    }

                    Value v = Pop();
                    Value index = Pop();
                    IIndexed indexed = Pop().AsIndexed;

                    indexed[index] = v;

                    Push(v);
                    break;
                }
                }
            }
        }

        private void BinaryOp(Op code) {
            if (!Peek(0).IsNumber || !Peek(1).IsNumber) {
                throw RuntimeError($"Both operands to '{code}' must be numbers.");
            }

            Value right = Pop();
            Value left = Pop();
            switch (code) {
            case Op.Add:
                Push(Value.FromNumber(left.AsNumber + right.AsNumber));
                break;
            case Op.Subtract:
                Push(Value.FromNumber(left.AsNumber - right.AsNumber));
                break;
            case Op.Multiply:
                Push(Value.FromNumber(left.AsNumber * right.AsNumber));
                break;
            case Op.Divide:
                Push(Value.FromNumber(left.AsNumber / right.AsNumber));
                break;
            case Op.Greater:
                Push(Value.FromBool(left.AsNumber > right.AsNumber));
                break;
            case Op.Less:
                Push(Value.FromBool(left.AsNumber < right.AsNumber));
                break;
            default:
                throw new Exception("Unreachable code.");
            }
        }

        private void CallValue(Value callee, int argCount) {
            switch (callee.Type) {
            case ValueType.Closure:
                CallClosure(callee.AsClosure, argCount);
                return;
            case ValueType.Native:
                CallNative(callee.AsNative, argCount);
                return;
            case ValueType.Class:
                CallClass(callee.AsClass, argCount);
                return;
            } 
            throw RuntimeError("Can only call functions and classes.");
        }

        private void CallClosure(ObjClosure closure, int argCount) {
            if (argCount != closure.Function.Arrity) {
                throw RuntimeError($"Expected {closure.Function.Arrity} arguments but got {argCount}");
            }

            if (_frameCount == FramesMax) {
                throw RuntimeError("Stack overflow (too many nested function calls).");
            }
            _frames[_frameCount++] = new CallFrame(closure, _stack, _stackTop - argCount - 1);
        }

        private void CallClass(ObjClass klass, int argCount) {
            _stackTop -= argCount + 1;
            Push(Value.FromInstance(new ObjInstance(klass)));
        }

        private void CallNative(ObjNative native, int argCount) {

            if (native.ArgCount != argCount) {
                throw RuntimeError("Expecting {native.ArgCount} arguments but got {argCount}.");
            }
            object?[] args = _stack[( _stackTop - argCount ).._stackTop].Select(v => v.AsObject()).ToArray();
            Value result;
            try {
                result = native.Function(args);
            } catch (Exception e) {
                throw RuntimeError(e.Message);
            }
            _stackTop -= argCount + 1;
            Push(result);
            return;
        }

        private ObjUpvalue CaptureUpvalue(Value local, int location) {
            ObjUpvalue? prev = null;
            ObjUpvalue? upvalue = _openUpvalues;

            while (upvalue != null && upvalue.Location > location) {
                prev = upvalue;
                upvalue = upvalue.Next;
            }

            if (upvalue != null && upvalue.Location == location) {
                return upvalue;
            }

            ObjUpvalue createdUpvalue = new(local, location) {
                Next = upvalue
            };

            if (prev == null) {
                _openUpvalues = createdUpvalue;
            } else {
                prev.Next = createdUpvalue;
            }

            return createdUpvalue;
        }

        private void CloseUpvalues(int last) {
            while (_openUpvalues != null && _openUpvalues.Location >= last) {
                ObjUpvalue? upvalue = _openUpvalues;
                _openUpvalues = upvalue.Next;
            }
        }

        private static bool IsFalsy(Value value) => value.IsNil || ( value.IsBool && !value.AsBool );

        private Value Peek(int distance) {
            return _stack[_stackTop - distance - 1];
        }

        RuntimeError RuntimeError(string message) {

            string result = "";

            for (int i = 0; i < _frameCount; i += 1) {
                CallFrame frame = _frames[i];
                ObjFunction func = frame.Closure.Function;
                // -1 because Ip always points at the
                // next instruction
                int offset = frame.Ip - 1;

                int line = func.Chunk.GetLine(offset);

                result += new string(' ', Math.Max(i, 8));
                result += $"at {func.Name ?? "Top Level"}:Line {line}{IReporter.NewLine}";
            }

            result += message;

            return new RuntimeError(result); 
        }

        private void Push(Value value) {
            _stack[_stackTop++] = value;
        }

        private Value Pop() {
            return _stack[--_stackTop];
        }

        private static bool ValuesEqual(Value a, Value b) {
            if (a.Type != b.Type) {
                return false;
            }

            switch (a.Type) {
            case ValueType.Bool: return a.AsBool == b.AsBool;
            case ValueType.Nil: return true;
            case ValueType.Number: return a.AsNumber == b.AsNumber;
            case ValueType.String: return a.AsString.Str.Equals(b.AsString.Str);
            default:
                throw new Exception("Unreachable code.");

            }
        }

        private class CallFrame {
            public CallFrame(ObjClosure closure, Value[] source, int offset) {
                Closure = closure;
                Slots = new ValueSlots(source, offset);
                Ip = 0;
            }

            public Chunk Chunk => Closure.Function.Chunk;
            public DynamicArray<byte> Code => Closure.Function.Chunk.Code;
            public DynamicArray<Value> Constants => Closure.Function.Chunk.Constants;
            public int Offset => Slots.Offset;

            public ObjClosure Closure { get; set; }
            public int Ip { get; set; }

            public ValueSlots Slots { get; set; }
        }
    }

    internal enum InterpretResult {
        Ok,
        CompileError,
        RuntimeError,
    }
}
