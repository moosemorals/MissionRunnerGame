namespace GameLibrary.Scripting {
    internal class ValueSlots {

        private readonly Value[] _values;
        private readonly int _offset;

        internal ValueSlots(Value[] source, int offset) {
            _values = source;
            _offset = offset;
        }

        internal Value this[int i] {
            get => _values[i + _offset];
            set => _values[i + _offset] = value;
        }

        internal int Count => _values.Length - _offset;

        internal int Offset => _offset;
    }
}
