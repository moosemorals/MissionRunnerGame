using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace GameLibrary.Scripting {

    internal class DynamicArray<T> : IEnumerable<T> {
        private int _count;
        private int _capacity;
        private T[] _values;

        internal DynamicArray() {
            _count = 0;
            _capacity = 0;
            _values = Array.Empty<T>();
        }

        internal int Push(T value) {
            if (_capacity < _count + 1) {
                _capacity = _capacity < 8 ? 8 : _capacity * 2;
                T[] newCode = new T[_capacity];
                Array.Copy(_values, newCode, _count);
                _values = newCode;
            }
            _values[_count++] = value;
            return _count - 1;
        }

        public IEnumerator<T> GetEnumerator() => _values.Take(_count).GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => throw new NotImplementedException();

        internal T this[int index] {
            get => _values[index];
            set => _values[index] = value;
        }

        internal int Length => _count;
    }
}
