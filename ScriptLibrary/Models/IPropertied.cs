﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GameLibrary.Scripting;

namespace GameLibrary.Models {
   public interface IPropertied {

        /// <summary>
        /// Get the named propery. Returns Value.Nil the property doesn't exist.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        Value GetProperty(string name);

        /// <summary>
        /// Sets the property. 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        void SetProperty(string name, Value value);

        /// <summary>
        /// Checks if a property exists. Returns true if the property exists,
        /// false otherwise.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        bool HasProperty(string name);
    }
}
