﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GameLibrary.Scripting;

namespace GameLibrary.Models {
    public interface IIndexed {
        Value this[Value i] { get; set; }
    }
}
