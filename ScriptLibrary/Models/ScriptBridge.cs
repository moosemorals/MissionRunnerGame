﻿using System;
using System.Collections.Generic;
using System.Reflection;

using GameLibrary.Scripting;

using ValueType = GameLibrary.Scripting.ValueType;

namespace GameLibrary.Models {

    public class ScriptBridge : IPropertied {

        private readonly object _source;
        private readonly Type _sourceType;
        private readonly IDictionary<string, PropertyInfo> _props;

        public ScriptBridge(object source) {
            _props = new Dictionary<string, PropertyInfo>();

            _source = source;
            _sourceType = source.GetType();

            PropertiesAttribute? pa =  source.GetType().GetCustomAttribute<PropertiesAttribute>();

            if (pa == null) {
                throw new ArgumentException("Source must have PropertiesAttribute");
            }

            foreach (string p in pa.Names) {
                PropertyInfo? pi = _sourceType.GetProperty(p);
                if (pi == null) {
                    throw new ArgumentException($"Type {_sourceType} does not have a property called {p}");
                }

                _props.Add(p, pi);
            }
        }

        public Value GetProperty(string name) {
            if (HasProperty(name)) {
                object? value = _props[name].GetValue(_source);

                return value switch {
                    double d => Value.FromNumber(d),
                    long d => Value.FromNumber(d),
                    string s => Value.FromString(new ObjString(s)),
                    Enum e => Value.FromString(new ObjString(e.ToString())),
                    IPropertied s => Value.FromInstance(s),
                    object o => Value.FromInstance(new ScriptBridge(o)),
                    _ => Value.FromNil(),
                };
            }
            return Value.FromNil();
        }
        
        public bool HasProperty(string name) => _props.ContainsKey(name);

        public void SetProperty(string name, Value value) {
            if (!HasProperty(name)) {
                return;
            }

            PropertyInfo pi = _props[name];

            if (value.Type == ValueType.String && pi.PropertyType == typeof(string)) {
                pi.SetValue(_source, value.AsString.Str);
            }
        }
    }

    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
    public class PropertiesAttribute : Attribute {

        public PropertiesAttribute(params string[] names) {
            Names = names;
        }

        public string[] Names { get; }
    }
}
