﻿using System;

using GameLibrary.Scripting;

namespace GameLibrary.Models {
    public interface IReporter {

        public readonly static string NewLine = Environment.NewLine;

        public void Print(Value value);
        public void Error(string message);
        public void Error(string message, Exception ex);


    }
}
