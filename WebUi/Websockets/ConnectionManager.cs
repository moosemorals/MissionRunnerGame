using System.Collections.Generic;
using System.Net.WebSockets;
using System.Threading.Tasks;

using GameLibrary.Engine;
using GameLibrary.Models.Events;

using Newtonsoft.Json;

using WebUi.Models;

namespace WebUi.Websockets {
    public class ConnectionManager {

        private int _connectionIdCounter;
        private readonly IDictionary<int, WebsocketConnection> _connections = new Dictionary<int, WebsocketConnection>();
        private readonly IDictionary<int, RemotePlayer> _players;

        public GameEngine Engine { get; private set; }

        public ConnectionManager(GameEngine engine) {
            Engine = engine;
            _players = new Dictionary<int, RemotePlayer>();
            LoadPlayers();
        }

        public void LoadPlayers() {

            _players.Add(1, new RemotePlayer(1, @"
fun OnProduction(e) { 
    print e.Type; 
}
fun OnTick(e) {
    print e.Type;
}
")); 
        }

        internal async Task Accept(WebSocket webSocket, int userId) {

            RemotePlayer player = _players[userId];

            TaskCompletionSource<object> done = new TaskCompletionSource<object>();
            WebsocketConnection conn = new WebsocketConnection(done, webSocket, _connectionIdCounter++);
            _connections.Add(conn.Id, conn);


            _ = Task.Run(() => conn.SendLoop().ConfigureAwait(false));
            _ = Task.Run(() => conn.ReceiveLoop().ConfigureAwait(false));

            player.AddConnection(conn);
            Engine.GameEventHandler += player.HandleGameEvent;
            await done.Task;
            Engine.GameEventHandler -= player.HandleGameEvent;
            player.RemoveConnection(conn);
        }
    }
}
