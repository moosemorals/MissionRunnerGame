using System;
using System.Net.WebSockets;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;

using Serilog;

namespace WebUi.Websockets {
    public class WebsocketMiddleware : IMiddleware {

        private readonly ConnectionManager _manager;
        private readonly ILogger _log = Log.ForContext<WebsocketMiddleware>();
        private readonly IDiagnosticContext _diagnosticContext;

        public WebsocketMiddleware(IDiagnosticContext diagnosticContext, ConnectionManager manager) {
            _diagnosticContext = diagnosticContext;
            _manager = manager;
        }

        public async Task InvokeAsync(HttpContext context, RequestDelegate next) {

            if (context.Request.Path == "/ws") {
                if (context.WebSockets.IsWebSocketRequest) {
                    int? userId = CheckAuth(context);

                    if (userId != null) {
                        _diagnosticContext.Set("UserId", userId);
                        using (WebSocket webSocket = await context.WebSockets.AcceptWebSocketAsync()) {
                            await _manager.Accept(webSocket, userId.Value);
                        }
                    }
                    return;
                }
                context.Response.StatusCode = 400;
            } else {
                await next(context);
            }
        }


        private static int? CheckAuth(HttpContext context) {
            return 1;
        }
    }
}
