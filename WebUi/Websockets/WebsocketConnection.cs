using System;
using System.Collections.Concurrent;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using GameLibrary.Engine;

namespace WebUi.Websockets {
    public class WebsocketConnection {

        private readonly TaskCompletionSource<object> _done;
        private readonly WebSocket _socket;
        private readonly BlockingCollection<string> _sendQueue = new();
        private readonly CancellationTokenSource _clientTokenSource = new CancellationTokenSource();

        public int Id { get; private set; }

        public WebsocketConnection(TaskCompletionSource<object> done, WebSocket webSocket, int id) {
            _socket = webSocket;
            _done = done;
            Id = id;
        }

        public void Send(string message) => _sendQueue.Add(message);

        public event EventHandler<string>? MessageReceived;

        public void Close() {
            if (!_clientTokenSource.IsCancellationRequested) {
                _clientTokenSource.Cancel();
            }
            if (_socket.State != WebSocketState.Closed) {
                _socket.Abort();
            }
            _done.SetResult(true);
        }

        internal async Task SendLoop() {
            CancellationToken token = _clientTokenSource.Token;
            try {
                while (!token.IsCancellationRequested) {
                    string message = _sendQueue.Take(token);

                    ArraySegment<byte> messageBuffer = new ArraySegment<byte>(Encoding.UTF8.GetBytes(message));

                    await _socket.SendAsync(
                        messageBuffer,
                        WebSocketMessageType.Text,
                        endOfMessage: true,
                        token
                    );
                }
            } catch (OperationCanceledException) {
                // Expected, not a problem.
            } finally {
                Close();
            }
        }

        public virtual void OnMessageRecieved(string message) {
            MessageReceived?.Invoke(this, message);
        }

        internal async Task ReceiveLoop() {
            try {
                CancellationToken token = _clientTokenSource.Token;
                ArraySegment<byte> buffer = WebSocket.CreateServerBuffer(4096);

                while (!token.IsCancellationRequested) {
                    WebSocketReceiveResult? recieved = await _socket.ReceiveAsync(buffer, token);
                    if (_socket.State == WebSocketState.CloseReceived && recieved.MessageType == WebSocketMessageType.Close) {
                        await _socket.CloseOutputAsync(WebSocketCloseStatus.NormalClosure, "Acknowlege Close", CancellationToken.None);
                        break;
                    }

                    if (buffer.Array != null) {
                        string message = Encoding.UTF8.GetString(buffer.Array, 0, recieved.Count);
                        OnMessageRecieved(message);
                    }
                }
            } catch (OperationCanceledException) {
                // Expected, ignored
            } finally {
                Close();
            }
        }
    }

}
