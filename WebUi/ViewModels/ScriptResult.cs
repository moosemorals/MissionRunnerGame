﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebUi.ViewModels {

    public class ScriptResult {
        [Required]
        public string? Script { get; set; }

        public ScriptStatus Status { get; set; } = ScriptStatus.NotRun;

        public string? PrintText { get; set; }

        public string? ErrorText { get; set; }

        public string? Code { get; set; }

        public Exception? Exception { get; set; }

    }
    public enum ScriptStatus {
        NotRun,
        Ok,
        CompileError,
        RuntimeError,
    }

}
