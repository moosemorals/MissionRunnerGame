﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using GameLibrary.Models;

namespace WebUi.ViewModels {
    public class StarSystemVM {

        public StarSystemVM(StarSystem model)  {
            Name = model.Name;

            Properties = new Dictionary<string, long> {
                { nameof(model.Population), model.Population },
                { nameof(model.Technology), model.Technology },
                { nameof(model.Environment), model.Environment }
            };

            Resources = model.SystemResources;

            Stockpile = model.Warehouses.ViewContents;

            Industries = model.Industries.Select(i => new IndustryVM(i)).ToList();
        }

        public string Name { get; }

        public IReadOnlyDictionary<string, long> Properties { get;  }

        public IReadOnlyList<Resource> Resources { get;  }

        public IReadOnlyDictionary<Resource, long> Stockpile { get; }

        public IReadOnlyList<IndustryVM> Industries { get; }
    }
}
