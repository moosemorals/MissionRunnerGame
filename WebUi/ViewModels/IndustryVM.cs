﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using GameLibrary.Models;

namespace WebUi.ViewModels {
    public class IndustryVM {

        public IndustryVM(Industry model) {
            Name = model.Name;
            Consumes = model.Consumes;
            Produces = model.Produces;

        }

        public string Name { get; }
        public IDictionary<Resource, long> Consumes { get; }
        public IDictionary<Resource, long> Produces { get; }

    }
}
