using System;

using GameLibrary.Engine;
using GameLibrary.Models;
using GameLibrary.Models.Industries;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

using Serilog;

using WebUi.Websockets;

namespace WebUi {
    public class Startup {

        public void ConfigureServices(IServiceCollection services) {

            GameEngine engine = BuildEngine();

            services.AddSingleton(engine);
            services.AddSingleton(new ConnectionManager(engine));
            services.AddTransient<WebsocketMiddleware>();

            services.AddControllersWithViews()
                .AddNewtonsoftJson(options => {
                    options.SerializerSettings.Converters.Add(new StringEnumConverter());
                    options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                });
        
            services.AddMvcCore()
                .AddMetricsCore();
        
        }



        public void Configure(IApplicationBuilder app, IWebHostEnvironment env) {
            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            }

            app.UseSerilogRequestLogging();

            app.UseStaticFiles();
            app.UseWebSockets(new WebSocketOptions {
                KeepAliveInterval = TimeSpan.FromSeconds(120),
            });

            app.UseMiddleware<WebsocketMiddleware>();

            app.UseRouting();

            app.UseEndpoints(endpoints => endpoints.MapControllers());
        } 

        private static GameEngine BuildEngine() {
            Model model = new Model();

            StarSystem prox = new StarSystem("Starter System", new Cords(0, 0), new[] { Resource.Metal, Resource.Food }) {
                Population = 1,
                Technology = 1,
            };

            prox.Industries.Add(new LuxuryGoodsIndustry(prox));
 
            model.Systems.Add(prox);

            GameEngine engine = new GameEngine(model);
            engine.Start();

            return engine;
        }
    }
}
