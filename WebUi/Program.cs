
using System;
using System.Linq;

using App.Metrics;
using App.Metrics.AspNetCore;
using App.Metrics.Formatters.Prometheus;

using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

using Serilog;
using Serilog.Events;
using Serilog.Formatting.Compact;

namespace WebUi {
    public class Program {
        public static int Main(string[] args) {

            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
                .MinimumLevel.Override("Microsoft.AspNetCore", LogEventLevel.Warning)
                .Enrich.FromLogContext()
                .WriteTo.Console(new RenderedCompactJsonFormatter())
                .CreateLogger();

            try {
                Log.Information("Starting web host");

                CreateHostBuilder(args).Build().Run();
                return 0;
            } catch (Exception ex) {
                Log.Fatal(ex, "Web host terminated unexpectedly");
                return 1;
            } finally {
                Log.CloseAndFlush();
            }

        }


        public static IHostBuilder CreateHostBuilder(string[] args) {
            var Metrics = AppMetrics.CreateDefaultBuilder()
                .OutputMetrics.AsPrometheusPlainText()
                .OutputMetrics.AsPrometheusProtobuf()
                .Build();



            return Host.CreateDefaultBuilder(args)
                .UseSerilog()
                .ConfigureMetrics(Metrics)
                .UseMetrics(options => {
                    options.EndpointOptions = eo => {
                        eo.MetricsTextEndpointOutputFormatter = Metrics.OutputMetricsFormatters
                            .OfType<MetricsPrometheusTextOutputFormatter>()
                            .First();
                        eo.MetricsEndpointOutputFormatter = Metrics.OutputMetricsFormatters
                            .OfType<MetricsPrometheusProtobufOutputFormatter>()
                            .First();
                    };

                })
                .ConfigureWebHostDefaults(webBuilder => webBuilder.UseStartup<Startup>());
        }
    }
}
