﻿
# System Properties

 * Population
 * Technology
 * Environment
 * Production

## Population

Population represents the number of people in the system. It's a rough log
scale, each level needs 10 times as much resources to support as the previous.

Higher populations can support better tech,  but need more resources to
maintain

Population consumes Food. Population growth/decline follows Food production
with a medium lag.

  * 0 None
  * 1 1-10 (scouting party)
  * 2 10-100 
  * 3 100-1k 
  * 4 1k-10k (established colony)
  * 5 10k - 100k 
  * 6 100k - 1m
  * 7 1m - 10m (self governing)
  * 8 10m - 100m
  * 9 100m - 1b
  * 10 1b - 10b

## Environment

Environment is a measure of how hard it is to live in a system. Higher values
are easier (give +ve multipliers), lower values are harder.

Investment can increase environment score, polution, war, revolution can
decrease (only to zero) 

Environment is improved by spending Metal in one off payments.

  * -1 Activly hostile
  *  0 None (Domes, full suits outside)
  *  1 Bad (Domes, breathers outside)
  *  2 Ok (surface living, breathable atmosphere)
  *  3 Good (surface living, compatible soils)
  *  4 Great (surface living, accelarated growth)

## Technology

Technology represents the ability of the system to produce complicated machinary.
It can be roughly equated to Earth history. Higher populations can support
a higher technology level, and a higher tech level can support higher 
populations.

Technology is maintianed by spending Metal. Tech levels above population
are much more expensive.

## Production

Production is a measure of how much food the System can produce. 

Production is automatic. Production is roughly 

    (Technology + Population) * Environment


# Dependencies

Technology and Environment both depend on Metal.
Popluation depends on Production.
Production depends on Technology, Population, and Environment.

