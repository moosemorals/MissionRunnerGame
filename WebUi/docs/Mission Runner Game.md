# Mission Runner Game

## Premise

As Emperess Supreme your control of the empire is absolute. You can direct
investment towards offense, defence, research, or even the welfare of your
people. You can instruct your mighty fleets to discover new worlds and
enslave new civilizations, or to concentrate on suppressing rival claimants
to your throne.

From your seat on the Thousand Year Throne you direct your forces, issuing
orders and receving reports.


## Resources

Ships - Move resources between systems, claim systems, fight other ships.
Systems - Provide resources, have owners, host ships

Primary industry
    Settlements ( -> people)
    Mines ( -> metal )
    Farms ( -> organics )

Secondary industry
    Shipyards ( metal -> ships )
    Factory ( organics -> food, metal -> luxury )
    Refinary ( organics, metal -> fuel )

Tertiary industry
    Academy (organics, people -> crew, farmers, workers)
    Spaceport ( organincs, metal, people ->, -> organics, metal, people )
    Entertainment ( luxury -> ) 

## Types

Ship
    Pair<System> Route
    System Location

    int Capacity
    int Crew
    int Health
    int Range
    int Fuel
    int Speed

System
    string Name
    Cords Location

    ICollection<Industry> Industries
    ICollection<Pair<Resource, int>> Resources
    ICollection<Pair<Resource, int>> Stockpile

Industry
    string Name
    IndustryType Type
    ICollection<Pair<Resource, int>> Consumes
    Pair<Resource,int> Produces
    int CycleTime

enum Resource
    metal
    organic
    people
    food
    luxury
    farmers
    engineers
    crew


enum IndustryType
    Primary
    Secondary
    Tertiary

## Engine

