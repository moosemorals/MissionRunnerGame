﻿
const Tau = Math.PI * 2;

export class Ant {
    /**
     * Create a new ant
     * @param {float} x Initial X cordinate
     * @param {float} y Initial Y cordinate
     * @param {float} h Initial heading in radians
     */
    constructor(x, y, h) {
        this.x = x;
        this.y = y;
        this.heading = h;
        this.t = 0;

        this.state = "none";
        this.deltaT = 0.05;
        this.scale = 10;

        this.stride = 1/32;
        this.legLength = 1;
        this.resolve;
        this.reject; 
    }

    move() {
        if (this.state == "none") {
            this.state = "moving";
            return new Promise((resolve, reject) => {
                this.resolve = resolve;
                this.reject = reject;
            });
        }
    }

    turnLeft() { 
        if (this.state == "none") {
            this.state = "turningLeft";
            return new Promise((resolve, reject) => {
                this.resolve = resolve;
                this.reject = reject;
            });
        } 
    }

    turnRight() { 
        if (this.state == "none") {
            this.state = "turningRight";
            return new Promise((resolve, reject) => {
                this.resolve = resolve;
                this.reject = reject;
            });
        } 
    }
 
    /**
     * Update internal state
     */
    tick(elapsed) {
        elapsed = this.deltaT;
        switch (this.state) {
            case "moving":
                const a = (this.heading * Tau);
                const b = Math.sin(Tau * this.stride) * this.legLength;
                this.x += b * Math.cos(a);
                this.y += b * Math.sin(a);
                break;
            case "turningLeft":
                this.heading -= (1 / 16) * elapsed;
                break;
            case "turningRight":
                this.heading += (1 / 16) * elapsed;
                break;
        }

        if (this.state != "none") {
            this.t += elapsed;
            if (this.t >= 2) {
                this.t = 0;
                const oldState = this.state;
                this.state = "none";
                this.resolve(oldState);
            }
        }
    }

    /**
     * Draw the ant at it's current cordinates and heading
     * @param {CanvasRenderingContext2D} g
     */
    draw(g) {
        const ll = this.legLength;

        function drawLeg(x, y, a) {
            g.beginPath();
            g.moveTo(x, y);
            g.lineTo(
                x + ll * Math.cos(a * Tau),
                y + ll * Math.sin(a * Tau)
            );
            g.stroke();
        }

        g.save();

        g.translate(this.x, this.y);
        g.scale(this.scale, this.scale);
        g.rotate((this.heading - 0.25) * Tau);

        // thorax
        g.strokeStyle = "white";
        g.fillStyle = "white";
        g.lineWidth = 0.1;
        g.beginPath();
        g.moveTo(-1, -1);
        g.lineTo(1, -1);
        g.lineTo(1, 1);
        g.lineTo(-1, 1);
        g.lineTo(-1, -1);
        g.closePath();
        g.fill();

        // legs
        const factor = (1 - 2 * (this.t - Math.floor(this.t))) * this.stride;
        if (this.t < 1) {
            drawLeg(-1, -0.5, 0.5 + factor); // Front
            drawLeg(1, 0, 0 - factor);  // back
            drawLeg(-1, 0.5, 0.5 + factor);  // middle

            drawLeg(1, -0.5, 0 + factor); // front
            drawLeg(-1, 0, 0.5 - factor);  // middle
            drawLeg(1, 0.5, 0 + factor); //back 
        } else {
            drawLeg(-1, -0.5, 0.5 - factor); // Front
            drawLeg(1, 0, 0 + factor);  // back
            drawLeg(-1, 0.5, 0.5 - factor);  // middle

            drawLeg(1, -0.5, 0 - factor); // front
            drawLeg(-1, 0, 0.5 + factor);  // middle
            drawLeg(1, 0.5, 0 - factor); //back 
        }
        g.restore();
    }
}
