﻿import { $, $$, append, emptyElement, frag, tag, toNode } from './common.js';

var systems;
var currentTick = 0

function getWebsocketAddress(path) {
    let result = location.protocol == "https" ? "wss://" : "ws://";
    result += location.host + "/" + path;
    return result;
}

async function fetchJson(path) {
    const res = await fetch(path);
    if (!res.ok) {
        log(`Couldn't GET ${path}: ${res.statusText}`);
        throw new Error("Failed to download");
    }
    return await res.json();
}

async function postJson(path, body) {
    const options = {
        method: "POST",
    };

    if (body != null) {
        options.headers = {
            "Content-Type": "application/json"
        };
        options.body = JSON.stringify(body);
    }

    const res = await fetch(path, options);
    if (!res.ok) {
        log(`Couldn't POST to ${path}: ${res.statusText}`)
        throw new Error("Failed to download");
    }
    if (res.status == 200) {
        return await res.json();
    }
    return;
}

function makeId(str) {
    return str.replace(/[^A-Za-z_-]/g, "");
}

function log(message) {
    const $log = $("#log");

    $log.appendChild(tag("div", null, tag("span", "tick", currentTick), ": ", message));
    $log.scrollTo(0, $log.scrollHeight);
}

function buildDisplayBox(summary, contents) {

    return tag("fieldset",  "box " + summary.toLowerCase(),
        tag("legend", null, summary),
        contents
    );
}

function buildResourceTable(pairs, clazz) {
    if (pairs == undefined) {
        pairs = {};
    }

    const $outer = tag("div", `grid-table ${clazz == null ? "" : clazz}`);

    const keys = Object.keys(pairs).sort();

    for (let i = 0; i < keys.length; i += 1) {
        const key = keys[i];
        const val = pairs[key];

        $outer.appendChild(tag("div", `resource ${key}`, key));
        $outer.appendChild(tag("div", `count ${key}`, val))
    }

    return $outer;
}

function buildIndustryDisplay(industry) {
    const $outer = tag("div", "industry");

    $outer.appendChild(tag("div", "title", industry.name));

    $outer.appendChild(buildResourceTable(industry.consumes, "consumes"));
    $outer.appendChild(buildResourceTable(industry.produces, "produces"));

    return $outer;
}

function buildIndustriesDisplay(industries) {
    const $outer = buildDisplayBox("Industries");

    for (let i = 0; i < industries.length; i += 1) {
        $outer.appendChild(buildIndustryDisplay(industries[i]))
    }

    return $outer;
}

function buildStockpileDisplay(stockpile) {
    return buildDisplayBox("Stockpile", buildResourceTable(stockpile));
}

function buildResourceDisplay(resources) {
    const $content = tag("div", "grid-table");

    for (let i = 0; i < resources.length; i += 1) {
        const r = resources[i];
        $content.appendChild(tag("div", `resource ${r}`, r));
        $content.appendChild(tag("div"));
    }

    return buildDisplayBox("Resources",  $content);
}

function buildSystemDetails(details) {

    return buildDisplayBox("Properties", buildResourceTable(details))
}

function buildSystem(system) {
    const name = system.name;
    const $system = tag("fieldset", { id: makeId("system-" + name), class: "system box" });

    $system.appendChild(tag("legend", null, name));

    $system.appendChild(buildSystemDetails(system.properties));
    $system.appendChild(buildResourceDisplay(system.resources));
    $system.appendChild(buildStockpileDisplay(system.stockpile));
    $system.appendChild(buildIndustriesDisplay(system.industries));

    return $system;
}

function buildDisplay() {

    const $systems = emptyElement($("#systems"));

    for (let name in systems) {
        $systems.appendChild(buildSystem(systems[name]));
    }
}

function connect() {
    const ws = new WebSocket(getWebsocketAddress("ws"));

    ws.onopen = e => log("Connected");
    ws.onclose = e => log(`Disconnected: (${e.code}) ${e.reason}`);
    ws.onerror = e => log("Error");
    ws.onmessage = messageHandler;

}

function messageHandler(e) {
    const payload = JSON.parse(e.data);

    if (!("Type" in payload)) {
        log("Badly formed message from the backend, ignoring");
        return;
    }

    switch (payload.Type) {
        case "Tick":
            currentTick = payload.Tick;
            append(emptyElement($("#tick-count")), currentTick);
            break;
        case "Industry":
            handleIndustryEvent(payload);
            break;
        case "Production":
            handleProductionEvent(payload);
            break;
        case "Print":
            log("Backend: " + payload.Message);
            break;
        case "Error":
            log("Backend error: " + payload.Message);
            break;
        default:
            log("Unexpected message from the backend");
    }
}

function ensureSystemKnown(name) {
    if (!(name in systems)) {
        systems[name] = {
            resources: {},
            industries: [],
            stockpile: {}
        };
    }

    let $system = $("#" + makeId("system-" + name));
    if ($system == null) {
        $system = buildSystem(systems[name]);
        $("#systems").appendChild($system);
    }

    return $system;
}

function flash($target, count) {

    function _tidyUp() {
        $target.removeEventListener("animationend",_tidyUp);
        $target.classList.remove("flash-border");
    }

    count = count || 3;
    $target.style.setProperty("--count", count);
    $target.addEventListener("animationend", _tidyUp);
    $target.classList.add("flash-border");

}

function updateStockpileDisplay($system, system) {

    const $stockpile = $($system, ".stockpile .grid-table");
    for (let r in system.stockpile) {
        const v = system.stockpile[r];

        let $dk = $($stockpile, `.resource.${r}`);
        let $dv = $($stockpile, `.count.${r}`);

        if ($dk == null) {
            $dk = tag("div", `resource ${r}`, r);
            $dv = tag("div", `count ${r}`, v);

            $stockpile.appendChild($dk);
            $stockpile.appendChild($dv);
        } else {
            append(emptyElement($dv), v);
        }
    }

    flash($stockpile, 1);
}

function handleProductionEvent(event) {
    const name = event.System;
    const $system = ensureSystemKnown(name); 
    const system = systems[name];

    if (!(event.Resource in system.stockpile)) {
        system.stockpile[event.Resource] = 0;
    }
    system.stockpile[event.Resource] += event.Count;

    updateStockpileDisplay($system, system);
}

function handleIndustryEvent(event) {
    const name = event.System;
    const $system = ensureSystemKnown(name);
    const system = systems[name];

    const industry = event.Industry;
    if (!system.industries.includes(industry)) {
        system.industries.push(industry);
    }

    const consumed = event.Consumed;
    if (consumed != null) {
        for (let r in consumed) {
            if (!(r in system.stockpile)) {
                system.stockpile[r] = 0;
            }
            system.stockpile[r] -= consumed[r];
        }
    }

    const produced = event.Produced;
    if (produced != null) {
        for (let r in produced) {
            if (!(r in system.stockpile)) {
                system.stockpile[r] = 0;
            }
            system.stockpile[r] += produced[r];
        }
    }

    updateStockpileDisplay($system, system); 
}

async function init() {

    const btn = $("#tick-cmd");
    btn.addEventListener("click", () => postJson("admin/Tick"));
    try {
        systems = await fetchJson("/state");
        buildDisplay();
        connect();
    } catch (ex) {
        console.error(ex);
        log(ex.message);
    }
}

addEventListener("DOMContentLoaded", init);