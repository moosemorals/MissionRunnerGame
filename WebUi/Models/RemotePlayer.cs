﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using GameLibrary.Engine;
using GameLibrary.Models;
using GameLibrary.Models.Events;
using GameLibrary.Scripting;

using Newtonsoft.Json;

using WebUi.Websockets;

namespace WebUi.Models {
    public class RemotePlayer {
        private readonly string _source;
        private readonly ObjFunction _topLevel;
        private readonly IList<WebsocketConnection> _connections;
        private readonly WebsocketReporter _reporter;

        public int Id { get; init; }

        public RemotePlayer(int id,  string source ) {
            _reporter = new WebsocketReporter(this);
            _connections = new List<WebsocketConnection>();
            _source = source;
            Id  = id;
            ObjFunction? compiled = new Compiler(_source, _reporter).Compile();
            if (compiled == null) {
                throw new RuntimeError("Script can't compile");
            }
            _topLevel = compiled;
        }

        public void AddConnection(WebsocketConnection conn) {
            lock (_connections) {
                _connections.Add(conn);
                conn.MessageReceived += HandleReceivedMessage;
            }
        }

        public void RemoveConnection(WebsocketConnection conn) {
            lock (_connections) {
                conn.MessageReceived -= HandleReceivedMessage;
                _connections.Remove(conn);
            }
        }

        public void HandleReceivedMessage(object? source, string message) {

        }

        public void SendMessage(object json) {
            SendMessage(JsonConvert.SerializeObject(json));
        }

        public void SendMessage(string message) {
            lock (_connections) {
                foreach (WebsocketConnection c in _connections) {
                    c.Send(message);
                }
            }
        }

        public void HandleGameEvent(object? source, GameEvent e) {
            switch (e) {
            case ProductionEvent pe: {
                ObjFunction? handler = _topLevel.GetChild("OnProduction");
                if (handler != null) {
                    handler.Call(_reporter, Value.FromInstance(new ScriptBridge(pe)));
                }
                break;
            }
            case TickEvent te: {
                ObjFunction? handler = _topLevel.GetChild("OnTick");
                if (handler != null) {
                    handler.Call(_reporter, Value.FromInstance(new ScriptBridge(te)));
                }
                break;
            }
            }
            SendMessage(e);
        } 

        private class WebsocketReporter : IReporter {

            private readonly RemotePlayer _player;

            public WebsocketReporter(RemotePlayer player) {
                _player = player;
            }

            public void Error(string message) => _player.SendMessage(new {Type= "Error", Message  = message});
            public void Error(string message, Exception ex) => _player.SendMessage(new { Type = "Error", Message = ex.Message });
            public void Print(Value value) => _player.SendMessage(new { Type = "Print", Message = value.ToString() });
        }
    }
}
