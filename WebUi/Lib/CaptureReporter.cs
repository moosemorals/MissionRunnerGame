﻿using System;
using System.IO;

using GameLibrary.Models;
using GameLibrary.Scripting;

namespace WebUi.Lib {
    public class CaptureReporter : IReporter {

        private readonly StringWriter _error = new();
        private readonly StringWriter _info = new();
        private readonly StringWriter _print = new();

        public void Error(string message) => _error.WriteLine(message);
        public void Error(string message, Exception ex) => _error.WriteLine(message);
        public void Info(string message) => _info.WriteLine(message);
        public void Print(Value value) => _print.WriteLine(value.ToString());


        public string? ErrorText => _error.ToString();
        public string? InfoText => _info.ToString();
        public string? PrintText => _print.ToString();

    }
}
