﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using GameLibrary.Engine;

using Microsoft.AspNetCore.Mvc;

namespace WebUi.Controllers {

    [Route("admin")]
    public class AdminController : Controller {

        private readonly GameEngine _engine;

        public AdminController(GameEngine engine) {
            _engine = engine;
        }

        [HttpPost("tick")]
        public IActionResult Tick() {
            _engine.Tick();

            return NoContent();
        }
    }
}
