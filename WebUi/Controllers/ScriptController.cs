﻿using System;

using GameLibrary.Scripting;

using Microsoft.AspNetCore.Mvc;

using WebUi.Lib;
using WebUi.ViewModels;

namespace WebUi.Controllers {

    [Route("/script")]
    public class ScriptController : Controller {

        [HttpGet("")]
        public IActionResult Index() {
            return View(new ScriptResult { Script = "" });
        }

        [HttpPost("")]
        public IActionResult Run(ScriptResult model) {

            if (!ModelState.IsValid) {
                return View("Index", model);
            }

            if (model.Script is null) {
                //Should have been caught by the validity check above
                throw new NullReferenceException();
            }

            CaptureReporter r = new CaptureReporter();

            ObjFunction? topLevel = new Compiler(model.Script, r).Compile();

            if (topLevel == null) {
                model.Status = ScriptStatus.CompileError;
                model.ErrorText = r.ErrorText;
                return View("Index", model);
            }

            string code = Disassembler.Disassemble(topLevel);
            try {
                new VM(r).Interpret(topLevel);
            } catch (Exception ex) {
                model.Status = ScriptStatus.RuntimeError;
                model.ErrorText = r.ErrorText;
                model.Exception = ex;
                return View("Index", model);
            }


            model.Status = ScriptStatus.Ok;
            model.PrintText = r.PrintText;
            model.Code = code;
            return View("Index", model);
        }
    }
}
