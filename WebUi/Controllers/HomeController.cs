
using System.Linq;

using GameLibrary.Engine;

using Microsoft.AspNetCore.Mvc;

using WebUi.ViewModels;

namespace WebUi.Controllers {
    public class HomeController : Controller {

        private readonly GameEngine _engine;

        public HomeController(GameEngine engine) {
            _engine = engine;
        }

        [HttpGet("")]
        //public IActionResult Index() => View(_engine);
        public IActionResult Index() => View("Ants");


        [HttpGet("/state")]
        [Produces("application/json")]
        public IActionResult State() {

            return Ok(_engine.Systems.Select(s =>new StarSystemVM(s) ).ToDictionary(s => s.Name));

        }
    }
}
